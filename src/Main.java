import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    private static volatile Queue <Car> carsQueue = new LinkedList<>();
    private static volatile boolean isBridgeOpened = true;
    private static volatile Queue <Car> cpQueue = new LinkedList<>();
    private static volatile int carsPassedBridge = 0;

    private static volatile int carsCPCount = 0;
    private static volatile int carsCPWeightSum = 0;
    private static final int maxCountOfCarsPassed = 20;

    public static void main(String[] args) {
        System.out.println("Available number of cores: " + Runtime.getRuntime().availableProcessors());

        Thread generateCars = new Thread(new AddNewCar());
        Thread checkPoint = new Thread(new CheckPoint());
        Thread bridge = new Thread(new Bridge());

        generateCars.start();
        checkPoint.start();
        bridge.start();

//        ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
//
//        service.execute(generateCars);
//        service.execute(checkPoint);
//        service.execute(bridge);
//
//        service.shutdown();


    }

    static class AddNewCar implements Runnable {
        Car currentCar;
        @Override
        public synchronized void run() {

            while (isBridgeOpened) {

                currentCar = new Car();

                carsQueue.offer(currentCar);

                try {
                    wait(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                notifyAll();
            }
            System.out.println("====== Cars generator stopped========");
            Thread.currentThread().interrupt();

        }
    }

     static class  CheckPoint implements Runnable {


        private Car currentCar;

        @Override
        public synchronized  void run() {
            Thread.currentThread().setName("Check Point");

            while (!Thread.currentThread().isInterrupted()) {
                if(carsQueue.size()>0) {
                    if (cpQueue.size() < 6 && (carsCPWeightSum + (carsQueue.peek() != null ? carsQueue.peek() != null ? carsQueue.peek().getWeight() : 0 : 0)) <= 20000) {

                        currentCar = carsQueue.poll();

                        carsCPCount++;
                        carsCPWeightSum += currentCar != null ? currentCar.getWeight() : 0;
                        cpQueue.offer(currentCar);
                        System.out.println(String.format("заехал на пункт - %s - %s , машин в очереди- %s, машин на пункте - %s, вес на пункте - %s",
                                currentCar.getId(), currentCar.getWeight(), carsQueue.size(), cpQueue.size(), carsCPWeightSum));
                        notifyAll();
                    }


                    if (carsPassedBridge > maxCountOfCarsPassed) {
                        isBridgeOpened = false;
                        System.out.println("===Passed 20 cars===");
                        Thread.currentThread().interrupt();

                    }
                }
            }


        }
    }

    static class Bridge implements Runnable {
        private Car currentCar;

        @Override
        public synchronized void run() {
            Thread.currentThread().setName("Bridge");
            Thread.currentThread().setPriority(9);

            while (!Thread.currentThread().isInterrupted()) {
                notifyAll();
                if (cpQueue.size() > 0) {
                    currentCar = cpQueue.poll();
                    assert currentCar != null;
                    System.out.println(String.format("выехал из пункта - %s - %s", currentCar != null ? currentCar.getId() : 0, currentCar.getWeight()));
                    carsCPCount--;
                    carsCPWeightSum -= currentCar.getWeight();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(String.format("заехал на мост - %s - %s", currentCar.getId(), currentCar.getWeight()));
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(String.format("выехал из моста - %s - %s машин на пункте - %s",
                            currentCar.getId(), currentCar.getWeight(), cpQueue.size()));
                    carsPassedBridge++;

                    if(carsPassedBridge>20 &&  cpQueue.size()==0){
                        System.out.println("====Bridge closed ====");
                        Thread.currentThread().interrupt();
                    }
                }

            }

        }
    }

}
