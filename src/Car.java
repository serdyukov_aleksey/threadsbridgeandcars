import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

class Car {
    private static final AtomicInteger count = new AtomicInteger(0);
    private int id;
    private int weight;

    Car (){
        Random rnd = new Random();
        this.id = count.incrementAndGet();
        this.weight = rnd.nextInt(20000);
    }

    int getId() {
        return id;
    }

    int getWeight() {
        return weight;
    }
}
